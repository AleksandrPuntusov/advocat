package com.advokat.kozarenko

import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.dialog_get_data.view.*
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity


class DialogGetData(mContext: Context) : AlertDialog(mContext) {

    internal var et_mail: EditText
    internal var et_phone: EditText
    internal var et_name: EditText

    private val mContext: Context

    init {
        this.mContext = mContext

        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.dialog_get_data, null)

        et_mail = view.et_mail
        et_phone = view.et_phone
        et_name = view.et_name

        view.bt_send_mail.setOnClickListener {
            if (et_mail.text.isEmpty() || et_name.text.isEmpty() || et_phone.text.isEmpty()) {
                Toast.makeText(mContext, "Для отправки запроса, заполните все поля", Toast.LENGTH_SHORT).show()
//               no do
            } else {
                val shareBody =
                        "Кому: 0675733245@kozarenko.net\n" +
                        "Я " + et_name.text +
                                ", прошу надати менi даннi на електронну адресу " + et_mail.text +
                                ", або з’вязатися зi мною за телефоном " + et_phone.text
                val sharingIntent = Intent(Intent.ACTION_SEND)
//
//                val mailto = "mailto:0675733245@kozarenko.net" +
//                        "?cc=" + et_mail.text +
//                        "&subject=" + Uri.encode("Надати менi даннi") +
//                        "&body=" + Uri.encode(shareBody)
//
////                val sharingIntent = Intent(Intent.ACTION_SENDTO)
//                sharingIntent.setData(Uri.parse(mailto));


                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_CC, "0675733245@kozarenko.net");
                sharingIntent.putExtra(Intent.EXTRA_EMAIL, "0675733245@kozarenko.net");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Надати менi даннi")
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
                mContext.startActivity(Intent.createChooser(
                        sharingIntent,
                        "0675733245@kozarenko.net"))
                dismiss()
            }
        }

        view.bt_cancel.setOnClickListener {
            dismiss()
        }

        this.setView(view)
    }

    override fun cancel() {}

}