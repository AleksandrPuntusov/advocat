package com.advokat.kozarenko

import android.webkit.WebViewClient
import android.webkit.WebView
import android.webkit.WebResourceRequest
import android.os.Build
import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity


class WebViewClientApp(webActivity: WebActivity) : WebViewClient() {

    private var webActivity: WebActivity

    init {
        this.webActivity=webActivity
    }

    @TargetApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        val toString = request.url.toString()
        if(toString.contains("tel:+380675733245")){
            makeCall()
            return true
        }

        view.loadUrl(toString)
        return true
    }

    // Для старых устройств
    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        view.loadUrl(url)
        return true
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
    }

    private fun makeCall() {
        if (webActivity != null) {
            webActivity.makeCall()
        }
    }
}