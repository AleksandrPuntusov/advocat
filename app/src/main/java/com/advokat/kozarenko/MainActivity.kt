package com.advokat.kozarenko

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import android.net.Uri.fromParts
import android.os.Handler
import android.util.Log
import android.view.View.GONE
import android.widget.Toast
import java.util.*

const val RESERVE_LINK = "https://outlook.office365.com/owa/calendar/Bookings@kozarenko.net/bookings/"
const val PAY_LINK = "https://www.liqpay.ua/ru/checkout/i4468383503"
const val MAP_LINK = "https://www.google.com.ua/maps/place/%D0%90%D0%B4%D0%B2%D0%BE%D0%BA%D0%B0%D1%82+%D0%9A%D0%BE%D0%B7%D0%B0%D1%80%D0%B5%D0%BD%D0%BA%D0%BE/@50.4378682,30.5118252,17z/data=!3m1!4b1!4m5!3m4!1s0x40d4cf5bf6c584ad:0xfb681f386070e9eb!8m2!3d50.4378648!4d30.5140139?hl=ru&shorturl=1"

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bt_reserve.setOnClickListener(this)
        bt_pay.setOnClickListener(this)
        bt_get_data.setOnClickListener(this)
        view_call.setOnClickListener(this)
        bt_map.setOnClickListener(this)

        Handler().postDelayed(Runnable {
            splash.visibility = GONE
        }, 2000)

    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.bt_reserve -> openWeb(RESERVE_LINK)
            R.id.bt_pay -> openWeb(PAY_LINK)
            R.id.bt_map -> openWeb(MAP_LINK)
            R.id.bt_get_data -> openDialog()
            R.id.view_call -> makeCall()
        }
    }

    private fun openWeb(s: String) {
        var intent: Intent = Intent(this, WebActivity::class.java)
        intent.putExtra("url", s)
        startActivity(intent)
    }

    private fun openDialog() {
        DialogGetData(this).show()
    }

    private fun makeCall() {
        val phone = "+380675733245"
        val phoneIntent = Intent(Intent.ACTION_DIAL,
                Uri.fromParts("tel", phone, null))
        startActivity(phoneIntent)
    }
}
