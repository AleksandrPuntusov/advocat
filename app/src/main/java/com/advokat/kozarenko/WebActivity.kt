package com.advokat.kozarenko

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity


class WebActivity : AppCompatActivity() {

    private var webView: WebView? = null
    private var url: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

         url = intent.extras.getString("url")

        webView = findViewById(R.id.web_view);
        // включаем поддержку JavaScript
        webView!!.getSettings().setJavaScriptEnabled(true);
        // указываем страницу загрузки
        webView!!.loadUrl(url);

        webView!!.setWebViewClient(WebViewClientApp(this))
    }


    override fun onBackPressed() {
        if (url.equals(PAY_LINK)) {
            super.onBackPressed()
        }

        if (webView!!.canGoBack()) {
            webView!!.goBack()
        } else {
            super.onBackPressed()
        }
    }

    public fun makeCall() {
        val phone = "+380675733245"
        val phoneIntent = Intent(Intent.ACTION_DIAL,
                Uri.fromParts("tel", phone, null))
        startActivity(phoneIntent)
    }
}
